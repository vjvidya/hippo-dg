import torch
import numpy as np
import math
import itertools
from config import EC2

final_list = []

class my_dictionary(dict):
    # __init__ function
    def __init__(self):
        self = dict()

    # Function to add key:value
    def add(self, key, value):
        self[key] = value

        # Main Function

on_prob = EC2.ACTIVATION_RATIO * EC2.NO_OF_NEURONS / 4

def binary_to_pattern(num: int, len):
    t = np.zeros((len,), dtype = int )
    counter = len
    while num != 0:
        # print(num)
        bit = math.ceil(num % 2)
        t[counter-1] = bit
        counter -= 1
        num = int(num / 2)

    return torch.from_numpy(t)

def gen_full():
    c = 0
    full_list = []
    for i in range(0, 1024):
        if check_count(i) == on_prob:
            c = c +1
            x = binary_to_pattern(i, 10)
            full_list.append(x)
        else:
            pass
    print ("total values: {0}".format(c))
    return full_list, c

def check_count(n):
    count = 0
    while (n):
        count += n & 1
        n >>= 1
    return count


def filter_out():
    full, c = gen_full()
    for i in range(0, c):
        print (full[i])

def create_dict():
    pat, c = gen_full()
    dict_obj = my_dictionary()
    for i in range(0, c):
        dict_obj.add(i, pat[i])

    return dict_obj, c

def create_pat():
    pairs, c = create_dict()
    print (pairs.get(5))
    for i in range(0,c-3,4):
        final = torch.cat((pairs.get(i), pairs.get(i+1), pairs.get(i+2), pairs.get(i+3)),0 )
        final_list.append(final)
        print (final_list)
    write_to_fil(final_list)

def create_pat_seed():
    pairs, c = create_dict()
    print (pairs.get(5))
    for i in range(0,c-3,4):

        final = torch.cat((pairs.get(i), pairs.get(i+1), pairs.get(i+2), pairs.get(i+3)),0 )
        final_list.append(final)
        print (final_list)
    write_to_fil(final_list)



def write_to_fil(data):
    file_name = "custo.txt"
    file_object = open(file_name, mode='w')
    curr_tensor_list = torch.stack(data, dim=1)
    curr_tensor_np = curr_tensor_list.numpy()
    curr_tensor_np = np.transpose(curr_tensor_np, (1, 0))
    np.savetxt(fname=file_name, X=curr_tensor_np, fmt="%d")
    file_object.close()

create_pat()











