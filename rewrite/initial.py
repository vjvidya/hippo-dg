import numpy
import torch
import random

from input_generator.binary_input_gen import BinaryInputGenerator
from input_generator.random_input import RIG
from hopfield_network import network, pattern_tools, plot_tools
from utils import populate_weights, create_zeroes, calc_sum
from config import HopfieldNetworkConfig as hnconfig
import config
import ojacode as oja
import dgcode as dgc


EC_Size = config.EC2.NO_OF_NEURONS
DG_Size = config.DentateGyrusConfig.OUTPUT_LAYER_NEURONS
CA3_Size = hnconfig.NO_OF_NEURONS
EC_DG_Dil = config.DentateGyrusConfig.DILUTION
DG_CA3_Dil = config.OjasConfig.DILUTION_DG_CA3
EC_CA3_Dil = config.OjasConfigEC2.DILUTION_EC2_CA3


import numpy as np
from typing import List, Union



def pattern_select(parameter: int) -> List:

    if parameter == 1:
        pattern_list = BinaryInputGenerator().read_from_file().pattern_list
        new_pattern_list = pattern_list

        return new_pattern_list

    if parameter == 2:
        pattern_list = RIG().read_from_fil().pattern_list
        new_pattern_list = pattern_list

        return new_pattern_list


def plot_graphs(pattern_list: List):

    #plot_tools.plot_pattern_list(pattern_list=pattern_list)
    overlap_matrix = pattern_tools.compute_overlap_matrix(pattern_list=pattern_list)
    plot_tools.plot_overlap_matrix(overlap_matrix=overlap_matrix)

    print(f"No of patterns = {hnconfig.NO_OF_PATTERN}")
    print(f"Average Overlap between patterns: {np.average(overlap_matrix)} \n")



def matrix_dilution_init(weight_matrix, iplayer_size, oplayer_size, dilution):
    nr_of_values = iplayer_size * oplayer_size
    no_of_zeros = int((1 - dilution) * nr_of_values)
    #no_of_zeros = int((1 - 0.9) * nr_of_values)

    if(no_of_zeros == 0):
        pass
    else:

        random.seed(3)
        x = random.sample(range(0, nr_of_values), no_of_zeros)
        weights = weight_matrix.numpy()
        for i in x:
            weights.flat[i] = 0
        weight_matrix = torch.from_numpy(weights)

    return weight_matrix


def weight_normalisation(weights, op_size):
    for j in range(0, op_size):
        jth_weight = j_th_weight(weights, j)
        updated_weights = weight_normalisation_jth(jth_weight)
        weights[:, j] = updated_weights
    return  weights

def weight_normalisation_jth(j_thweights):
    sum_ = calc_sum(j_thweights)
    j_thweights /= sum_
    return j_thweights

def j_th_weight(weight, j):
    j_th_column = weight[:, j]
    return j_th_column

def weight_init_ec_ca3():

    Weights = create_zeroes(EC_Size, CA3_Size)

    # for i in range(0, EC_Size):
    #     for j in range(0, CA3_Size):
    #         Weights[i][j] = (1/((EC_Size * CA3_Size) * EC_CA3_Dil))

    geights = 1.0 / EC_Size * (abs(2 * np.random.rand(EC_Size, CA3_Size) - 1))
    print(geights)
    print("ec size")

    Weights = torch.FloatTensor(geights)


    Weights = matrix_dilution_init(Weights, EC_Size, CA3_Size, EC_CA3_Dil)
    Weights = weight_normalisation(Weights, CA3_Size)
    print (Weights)
    return Weights

def weight_init_dg_ca3():

    Weights = create_zeroes(DG_Size, CA3_Size)

    # for i in range(0, DG_Size):
    #     for j in range(0, CA3_Size):
    #         Weights[i][j] = (1 / ((DG_Size * CA3_Size) * DG_CA3_Dil))

    geights = 1.0 / DG_Size * (abs(2 * np.random.rand(DG_Size, CA3_Size) - 1))

    Weights = torch.FloatTensor(geights)

    Weights = matrix_dilution_init(Weights, DG_Size, CA3_Size, DG_CA3_Dil)
    Weights = weight_normalisation(Weights, CA3_Size)

    return Weights

    return Weights

def weight_init_ec_dg():
    #
    Weights = create_zeroes(EC_Size, DG_Size)

    # for i in range(0, EC_Size):
    #     for j in range(0, DG_Size):
    #         Weights[i][j] = (1 / ((EC_Size * DG_Size) * EC_DG_Dil))

    geights = 1.0 / EC_Size * (abs(2 * np.random.rand(EC_Size, DG_Size) - 1))

    Weights = torch.FloatTensor(geights)

    Weights = matrix_dilution_init(Weights, EC_Size, DG_Size, EC_DG_Dil)
    Weights = weight_normalisation(Weights, DG_Size)

    return Weights

    return Weights

def from_ec2_ca3(pattern, weight):
    p = reconvert(pattern)
    #print(p)
    activation = oja.activation_function(p, weight, EC_Size, CA3_Size)
    return activation

def from_ec2_dg_ca3(pattern, ecdgweight, dgca3weight, oplayer):

    dgactivation = dgc.run_dem(pattern, ecdgweight, oplayer)
    dg_oplayer = dgc.extract_activation_list(dgactivation, config.DentateGyrusConfig.OUTPUT_ACTIVATION_RATIO)
    ecdgweight = dgc.weight_change(pattern, ecdgweight, dg_oplayer)
    ca3activation = oja.activation_function(dg_oplayer, dgca3weight, DG_Size, CA3_Size)

    return ca3activation, ecdgweight, dg_oplayer


def network_recall(input_pattern, no_of_flips, weight):

    if no_of_flips == 0:
        noisy_init_state = input_pattern
    else:
        noisy_init_state = pattern_tools.flip_n(input_pattern, no_of_flips)
    n = reconvert(noisy_init_state)
    activation = oja.activation_function(n, weight, EC_Size, CA3_Size)
    a = torch.FloatTensor(activation)
    error_pattern = oja.extract_activation_list(a, hnconfig.OUTPUT_ACTIVATION_RATIO)
    p = convert(error_pattern)

    return p

def flip_bits(template, nr_of_flips):
    """
    makes a copy of the template pattern and flips
    exactly n randomly selected states.
    Args:
        template:
        nr_of_flips:
    Returns:
        a new pattern
    """
    n = np.prod(template.shape)
    # pick nrOfMutations indices (without replacement)
    idx_reassignment = np.random.choice(n, nr_of_flips, replace=False)
    linear_template = template.flatten()
    linear_template[idx_reassignment] = -linear_template[idx_reassignment]
    return linear_template.reshape(template.shape)


def convert(pattern):
    p_np = pattern.numpy()
    p_flat = p_np.flatten()
    for i in range(0, len(p_flat)):
        if p_flat[i] == 0:
            p_flat[i] = -1
    return torch.from_numpy(p_flat)

def reconvert(pattern):
    p_np = pattern
    p_flat = p_np.flatten()
    for i in range(0, len(p_flat)):
        if p_flat[i] == -1:
            p_flat[i] = 0
    return (p_flat)


def check_similarity(a,b):
    c = 0
    t = 0
    for i in range(0, a.shape[0]):
        for j in range(0, a.shape[1]):
            # print ("{0},{1}".format(i,j))
            if (a[i][j] == 1):
                t = t + 1
                if (a[i][j] == b[i][j]):
                    c = c + 1

            else:
                pass
    #print (t)
    #print (c)
    m = float(c/t)
    return m



if __name__ == '__main__':
    pass