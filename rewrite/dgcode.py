import torch, random
from utils import create_activation_list, calc_max, ReLu, \
    calc_sum, calc_mean, create_op_zeroes, populate_weights_uniform, input_set, populate_weights
from config import DentateGyrusConfig as dg_config
from config import EC2
import initial

iplayer_size = EC2.NO_OF_NEURONS
oplayer_size = dg_config.OUTPUT_LAYER_NEURONS
alpha = dg_config.ALPHA
oplayer = create_op_zeroes(oplayer_size)



def weight_normalisation(ecdgweight):
    for j in range(0, oplayer_size):
        jth_weight = j_th_weight(ecdgweight, j)
        updated_weights = weight_normalisation_jth(jth_weight)
        ecdgweight[:, j] = updated_weights
    return  ecdgweight

def weight_normalisation_jth(j_thweights):
    sum_ = calc_sum(j_thweights)
    j_thweights /= sum_
    return j_thweights

def j_th_weight(ecdgweight, j):
    j_th_column = ecdgweight[:, j]
    return j_th_column

def extract_max(input_array):
    full_list = []
    for val in input_array:
        full_list.append(val.item())

    return max(full_list)

def check_if_computation(pattern, jth_weight):
    sum_ = 0
    for i in range(0, iplayer_size):
        #print(jth_weight[i])
        #print(pattern[i])
        sum_ += jth_weight[i].item() * pattern[i].item()

    #print (sum_)

    if sum_ > 0.1:

        #print("true")
        return True
    else:
        #print("false")
        return False

def inner_value(ecdgweight, oplayer, i):
    result_list = []
    for k in range(0, oplayer_size):

        curr_weight_row = j_th_weight(ecdgweight, k)
        max_weight = calc_max(curr_weight_row)
        #print(max_weight.item)
        temp_curr_value = ( ecdgweight[i, k] / max_weight)
        max_op = calc_max(oplayer).item()
        if max_op == 0:
            max_op = 1
        curr_2 = (oplayer[k] / max_op).item()
        #print(curr_2)
        curr_value = temp_curr_value * curr_2
        result_list.append(curr_value)

    return extract_max(input_array=result_list)

def calc_y_j(pattern, oplayer, ecdgweight, jth_weight):
    result_list = []
    for i in range(0, iplayer_size):
        #print ("inside for")
        temp_curr_value = alpha * inner_value(ecdgweight, oplayer, i)
        curr_value = ReLu(1 - temp_curr_value)
        #print("curr_value")
        #print(curr_value)

        interim_result = jth_weight[i].item() * pattern[i].item() * curr_value
        result_list.append(interim_result)

    #print(result_list)
    result_list = torch.FloatTensor(result_list)
    #print(result_list)
    return calc_sum(result_list)

def weight_change_mod(pattern, ecdgweight, oplayer):
    for i in range(0, iplayer_size):

        for j in range(0, oplayer_size):
            if (ecdgweight[i, j] == 0):
                pass

            else:
                ecdgweight[i, j] += alpha * oplayer[j].item() * \
                                            (pattern[i].item() - oplayer[j].item() *
                                             ecdgweight[i, j])
    weight_normalisation(ecdgweight)
    return ecdgweight

def weight_change(iplayer, ecdgweight, oplayer):

    print("inside dg wt update")

    iplayer = torch.FloatTensor(iplayer)
    oplayer = torch.FloatTensor(oplayer)

    ip_sum = calc_sum(iplayer)
    op_sum = calc_sum(oplayer)

    ip_mean = calc_mean(iplayer.float())
    op_mean = calc_mean(oplayer)

    for i in range(0, iplayer_size):
        left_val = dg_config.BETA * ( (iplayer[i].float() - ip_mean.float() ) / ip_sum.float())
        for j in range(0, oplayer_size):
            if (ecdgweight[i, j] == 0):
                ecdgweight[i, j] = 0
                pass

            else:
                right_val = ReLu( oplayer[j] - op_mean) / op_sum
                delta_change = left_val * right_val
                ecdgweight[i,j] += delta_change

    ecdg = weight_normalisation(ecdgweight)
    return ecdg

def run_dem(pattern, ecdgweight, oplayer):

    result_list = []

    p_flat = initial.reconvert(pattern)

    for j in range(0, oplayer_size):
        jth_weight = j_th_weight(ecdgweight, j)
        if_compute = check_if_computation(p_flat, jth_weight)
        if if_compute:
            result_list.append(calc_y_j(p_flat, oplayer, ecdgweight, jth_weight))

        else:
            result_list.append(torch.FloatTensor([0]))

    interim_list = []
    for result in result_list:
        interim_list.append(result.item())
    op = torch.FloatTensor(interim_list)
    return op


def oplayer_dict(oplayer):
    oplayer_list = []
    for idx, value in enumerate(oplayer):
        temp_dict = dict()
        temp_dict['idx'] = idx
        temp_dict['value'] = value.item()
        oplayer_list.append(temp_dict)
    return oplayer_list

def extract_activation_list(oplayer, activation_ratio):

    sorted_oplayer = sorted(oplayer_dict(oplayer), key=lambda k: k['value'], reverse=True)
    no_of_elem = int(activation_ratio * oplayer_size)
    sorted_oplayer = sorted_oplayer[:no_of_elem]
    activated_indices = []

    for data in sorted_oplayer:
        activated_indices.append(data['idx'])

    for idx, value in enumerate(oplayer):
        if idx in activated_indices:
            oplayer[idx] = 1
        else:
            oplayer[idx] = 0

    return oplayer


if __name__ == '__main__':
    #RUNNING SHIFTED TO dg_run.py
    pass



