from hopfield_network import network, pattern_tools, plot_tools
from config import HopfieldNetworkConfig as hnconfig
from config import EC2
from config import DentateGyrusConfig as dg
from config import OjasConfigEC2, OjasConfig
import initial, utils
import torch
import dgcode as dgc
import ojacode as oja


import numpy as np
from typing import List, Union


two_dimesional_vector = np.ndarray


def run_program() -> List[two_dimesional_vector]:

    #initialize hopfield network
    hopfield_net = network.HopfieldNetwork(nr_neurons=hnconfig.NO_OF_NEURONS)
    pattern_factory = pattern_tools.PatternFactory(hnconfig.PATTERN_FACTORY_LENGTH, hnconfig.PATTERN_FACTORY_WIDTH)

    #Create pattern list and plot
    pattern_list = initial.pattern_select(parameter=2)
    initial.plot_graphs(pattern_list=pattern_list)
    print("Pattern list generated")


    #Initialize weights
    EC_CA3 = initial.weight_init_ec_ca3()
    DG_CA3 = initial.weight_init_dg_ca3()
    EC_DG = initial.weight_init_ec_dg()
    print("Weights initialized")
    oplayer = utils.create_op_zeroes(dg.OUTPUT_LAYER_NEURONS)
    activated_patterns: List = list()
    c = 0

    #f = open("results.txt", "a+")
    #f.write("No. of neurons: \t ECII = {0} \t CA3 ={1} \t DG = {2}".format(EC2.NO_OF_NEURONS, hnconfig.NO_OF_NEURONS, dg.OUTPUT_LAYER_NEURONS))
    #f.write("Dilution values: \t Hopfield = {0} \t DG_CA3 = {1} \t EC_CA3 = {2} \t EC_DG = {3} \t acti at 0.40".format(
     #   hnconfig.DILUTION, OjasConfig.DILUTION_DG_CA3, OjasConfigEC2.DILUTION_EC2_CA3, dg.DILUTION))


    for pattern in pattern_list:
        #Calculate activation at CA3 from EC2 and DG
        activation1 = initial.from_ec2_ca3(pattern, EC_CA3)
        activation2, EC_DG, oplayer = initial.from_ec2_dg_ca3(pattern, EC_DG, DG_CA3, oplayer)
        a1 = torch.FloatTensor(activation1)
        a2 = torch.FloatTensor(activation2)
        #print ("at ca3, tensors")
        #print (a1)
        #print (a2)
        Final_CA3_Activation = a1 + a2
        #Final_CA3_Activation = a1

        #Final activation at CA3
        CA3_acti_list = oja.extract_activation_list(Final_CA3_Activation, hnconfig.OUTPUT_ACTIVATION_RATIO)

        #Update weights


        #EC_CA3 = oja.weight_update(pattern, EC_CA3, CA3_acti_list)
        DG_CA3 = oja.weight_update(oplayer, DG_CA3, CA3_acti_list)

        To_CA3 = initial.convert(CA3_acti_list)

        activated_patterns.append(To_CA3.numpy())
        c = c + 1
        print("Pattern Done {0}".format(c))

    #print (activated_patterns)
    hopfield_net.store_patterns(pattern_list=activated_patterns)
    plot_tools.plot_nework_weights(hopfield_network=hopfield_net)



    #modified_list = activated_patterns
    #del activated_patterns

    print("Testing with {0} patterns and {1} neurons".format(hnconfig.NO_OF_PATTERN, hnconfig.NO_OF_NEURONS))



    no_of_flips = int(EC2.ERROR_RATE * EC2.NO_OF_NEURONS / 100)
    print ("no of flips = {0}".format(no_of_flips))

    recall_value = initial.network_recall(pattern_list[0], no_of_flips, EC_CA3)

    hopfield_net.set_state_from_pattern(recall_value.numpy())
    hopfield_net.set_dynamics_sign_async()
    states = hopfield_net.run_with_monitoring(nr_steps=5)
    states_as_patterns = pattern_factory.reshape_patterns(states)
    modified_as_patterns = pattern_factory.reshape_patterns(activated_patterns)


    m = initial.check_similarity(states_as_patterns[4], modified_as_patterns[0])
    #print(states_as_patterns[4])
    #print(modified_as_patterns[0])
    print("Error rate = {0}% and Similarity = {1}".format(EC2.ERROR_RATE, m))

    plot_tools.plot_pattern_list([states_as_patterns[0], modified_as_patterns[0]])

    title = f"Testing with {EC2.ERROR_RATE}% error and " \
            f"{hnconfig.NO_OF_PATTERN} patterns with {hnconfig.NO_OF_NEURONS} neurons"

    plot_tools.plot_state_sequence_and_overlap(states_as_patterns, modified_as_patterns, reference_idx=0,
                                               suptitle=title)

    f = open("results.txt", "a+")
    #f.write("ECII  CA3   DG   Hopfield DG_CA3 EC_CA3 EC_DG NP Flips Sim \n")
    f.write("\n{0} {1:5} {2:6} {3:7} {4:6} {5:6} {6:6} {7:6} {8:6} {9:6} acti ratio = {10} sum > 0.1, ec3 dis,only positive wt".format(EC2.NO_OF_NEURONS,
                hnconfig.NO_OF_NEURONS,dg.OUTPUT_LAYER_NEURONS, hnconfig.DILUTION,OjasConfig.DILUTION_DG_CA3,
                OjasConfigEC2.DILUTION_EC2_CA3,dg.DILUTION, hnconfig.NO_OF_PATTERN, no_of_flips, m, EC2.ACTIVATION_RATIO))
    f.close()

    print("lowered activation of ca3 to .30")

    return 1


if __name__ == '__main__':
    run_program()