import random
import torch
import numpy
from config import OjasConfig, OjasConfigEC2
from config import HopfieldNetworkConfig as hn
from utils import populate_weights_uniform, calc_sum, create_zeroes
from typing import List



def j_th_weight(weights, j):
    j_th_column = weights[:, j]
    return j_th_column

def weight_normalisation_jth(j_thweights):
    sum_ = calc_sum(j_thweights)
    j_thweights /= sum_
    return j_thweights

def weight_normalisation(weight, iplayer, oplayer):
    for j in range(0, len(oplayer)):
        jth_weight = j_th_weight(weight, j)
        updated_weights = weight_normalisation_jth(jth_weight)
        weight[:, j] = updated_weights
    return  weight



def activation_function(pattern, weights, iplayer_size, oplayer_size):
    result_list = []
    #print("inside activations")
    #print(weights)
    for j in range(0, oplayer_size):
        jth_weights = j_th_weight(weights, j)
        #print (weights)
        #print (jth_weights)
        curr_weight = activate_a_node(jth_weights, iplayer_size, pattern)
        result_list.append(curr_weight)

    #self.oplayer = torch.FloatTensor(result_list)
    return result_list

def activate_a_node(jth_weights, iplayer_size, pattern):
    sum_ = 0
    for i in range(0, iplayer_size):
        sum_ += jth_weights[i].item() * pattern[i].item()

    return sum_

def weight_update(iplayer, weight, oplayer):

    for i in range(0, len(iplayer)):
        for j in range(0, len(oplayer)):
            if ((weight[i, j]) == 0):
                weight[i, j] = 0
                pass
            else:
                weight[i, j] += OjasConfig.ALPHA * oplayer[j].item() * \
                                        (iplayer[i].item() - oplayer[j].item() *
                                         weight[i,j])
    wt = weight_normalisation(weight, iplayer, oplayer)
    return wt


def oplayer_dict(oplayer):
    oplayer_list = []
    for idx, value in enumerate(oplayer):
        temp_dict = dict()
        temp_dict['idx'] = idx
        temp_dict['value'] = value.item()
        oplayer_list.append(temp_dict)
    return oplayer_list

def extract_activation_list(oplayer, activation_ratio):

    sorted_oplayer = sorted(oplayer_dict(oplayer), key=lambda k: k['value'], reverse=True)
    no_of_elem = int(activation_ratio * len(oplayer))
    sorted_oplayer = sorted_oplayer[:no_of_elem]
    activated_indices = []

    for data in sorted_oplayer:
        activated_indices.append(data['idx'])

    for idx, value in enumerate(oplayer):
        if idx in activated_indices:
            oplayer[idx] = 1
        else:
            oplayer[idx] = 0

    return oplayer

if __name__ == '__main__':
    pass