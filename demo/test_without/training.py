from hopfield_network import network, pattern_tools, plot_tools
import numpy
import time
nr_of_flips= 1
nr_patterns= 7
nr_neurons= 100 #Number of neurons in CA3


hopfield_net = network.HopfieldNetwork(nr_neurons=nr_neurons)

factory = pattern_tools.PatternFactory(pattern_length=10, pattern_width=10)

pattern_list = factory.create_random_pattern_list(nr_patterns, on_probability=0.40)

plot_tools.plot_pattern_list(pattern_list)

overlap_matrix = pattern_tools.compute_overlap_matrix(pattern_list)
plot_tools.plot_overlap_matrix(overlap_matrix)
print("average overlap:",numpy.average(overlap_matrix))

hopfield_net.store_patterns(pattern_list)
plot_tools.plot_nework_weights(hopfield_net)


#constant error rate testing

for j in range(1,4):
    noisy_init_state = pattern_tools.flip_n(pattern_list[0], nr_of_flips)
    print("noisy pattern \n {0}".format(noisy_init_state))
    hopfield_net.set_state_from_pattern(noisy_init_state)
    hopfield_net.set_dynamics_sign_async()
    states = hopfield_net.run_with_monitoring(nr_steps=4)
    print("Run {0} with {1} flips".format(j, nr_of_flips))
    states_as_patterns = factory.reshape_patterns(states)
    plot_tools.plot_state_sequence_and_overlap(states_as_patterns, pattern_list, reference_idx=0,
                                               suptitle="Testing with {0} flips and {1} patterns with {2} neurons".format(
                                                   nr_of_flips, nr_patterns, nr_neurons))

#same network with varying error rates
#
# for j in range(1, 2):
#     for i in (10,20,30,40,50,60,70):
#         noisy_init_state = pattern_tools.flip_n(pattern_list[0], i)
#         hopfield_net.set_state_from_pattern(noisy_init_state)
#         hopfield_net.set_dynamics_sign_async()
#         start_time = time.time()
#         states = hopfield_net.run_with_monitoring(nr_steps=4)
#         print("Run {0} with {1} flips took {2} secs".format(j, i, time_taken))
#         plot_tools.plot_state_sequence_and_overlap(states_as_patterns, pattern_list, reference_idx=0,
#                                                    suptitle="Testing with {0} flips and {1} patterns with {2} neurons".format(
#                                                        i, nr_patterns, nr_neurons))
