import torch

from config import HopfieldNetworkConfig as hn
from config import OjasConfig as oj_config
from config import DentateGyrusConfig as dgconfig
from dendrate_gyrus.dg import DendrateGyrus
from utils import populate_weights_uniform, calc_sum


class OjasRule(): # TODO: Change the way input is given to DG also

    def __init__(self, dg: DendrateGyrus):
        self.config = oj_config
        self.iplayer = dg.oplayer
        self.iplayer_size = len(dg.oplayer)
        self.oplayer_size = hn.NO_OF_NEURONS
        self.oplayer = None
        self.weight_matrix = populate_weights_uniform(self.iplayer_size, self.oplayer_size)
        self.weight_normalisation()

    def j_th_weight(self, j):
        j_th_column = self.weight_matrix[:, j]
        return j_th_column

    def weight_normalisation_jth(self, j_thweights):
        sum_ = calc_sum(j_thweights)
        j_thweights /= sum_
        return j_thweights

    def weight_normalisation(self):
        for j in range(0, self.oplayer_size):
            jth_weight = self.j_th_weight(j)
            self.weight_normalisation_jth(j_thweights=jth_weight)

    def activation_function(self):
        result_list = []
        for j in range(0, self.oplayer_size):
            jth_weights = self.j_th_weight(j)
            curr_weight = self.activate_a_node(jth_weights=jth_weights)
            result_list.append(curr_weight)

        self.oplayer = torch.FloatTensor(result_list)

    def activate_a_node(self, jth_weights):
        sum_ = 0
        for i in range(0, self.iplayer_size):
            sum_ += jth_weights[i].item() * self.iplayer[i].item()

        return sum_

    def weight_update(self):
        for i in range(0, self.iplayer_size):
            for j in range(0, self.oplayer_size):
                self.weight_matrix[i, j] += self.config.ALPHA * self.oplayer[j].item() * \
                                            (self.iplayer[i].item() - self.oplayer[j].item() *
                                             self.weight_matrix[i,j])

    @property
    def oplayer_dict(self):
        oplayer_list = []
        for idx, value in enumerate(self.oplayer):
            temp_dict = dict()
            temp_dict['idx'] = idx
            temp_dict['value'] = value.item()
            oplayer_list.append(temp_dict)
        return oplayer_list

    def extract_activation_list(self):

        sorted_oplayer = sorted(self.oplayer_dict, key=lambda k: k['value'], reverse=True)
        no_of_elem = int(self.config.OUTPUT_ACTIVATION_RATIO * self.oplayer_size)
        sorted_oplayer = sorted_oplayer[:no_of_elem]
        activated_indices = []

        for data in sorted_oplayer:
            activated_indices.append(data['idx'])

        for idx, value in enumerate(self.oplayer):
            if idx in activated_indices:
                self.oplayer[idx] = 1
            else:
                self.oplayer[idx] = 0


if __name__ == '__main__':
    pass