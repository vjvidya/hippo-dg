import random
import torch
import numpy
from config import OjasConfig, OjasConfigEC2
from config import HopfieldNetworkConfig as hn
from utils import populate_weights_uniform, calc_sum, create_zeroes
from typing import List

class OjasRule():

    __slots__ = ["config", "dilution", "iplayer", "iplayer_size", "oplayer", "oplayer_size", "weight_matrix", "eli"]

    def __init__(self, input_pattern: torch.FloatTensor, source_of_entry: int):
        """
            Function for initialising the Ojas Network

            :param input_pattern: Pattern Vector of the patterns in a single layer.
            :param source_of_entry: Where the pattern list is coming from. 1 for CA3, 2 for EC2.

        """

        if source_of_entry == 1:

            self.config = OjasConfig
            self.dilution = self.config.DILUTION_DG_CA3
            self.eli = self.config.ELIMINATION
            self.iplayer = input_pattern
            self.iplayer_size = len(input_pattern)
            self.oplayer_size = hn.NO_OF_NEURONS
            self.oplayer = None
            self.weight_matrix = create_zeroes(self.iplayer_size, self.oplayer_size)
            self.weight_initialisation()


        elif source_of_entry == 2:

            self.config = OjasConfigEC2
            self.dilution = self.config.DILUTION_EC2_CA3
            self.eli = self.config.ELIMINATION
            self.iplayer = input_pattern
            self.iplayer_size = len(input_pattern)
            self.oplayer_size = hn.NO_OF_NEURONS
            self.oplayer = None
            self.weight_matrix = create_zeroes(self.iplayer_size, self.oplayer_size)
            self.weight_initialisation()



    def weight_initialisation(self):
        print("inside oja init")
        for i in range(0, self.iplayer_size):
            for j in range(0, self.oplayer_size):
                self.weight_matrix[i][j] = (1 / (self.iplayer_size * self.oplayer_size) * self.dilution)
        self.matrix_dilution_init()

    def j_th_weight(self, j):
        j_th_column = self.weight_matrix[:, j]
        return j_th_column

    def weight_normalisation_jth(self, j_thweights):
        sum_ = calc_sum(j_thweights)
        j_thweights /= sum_
        return j_thweights

    def weight_normalisation(self):
        for j in range(0, self.oplayer_size):
            jth_weight = self.j_th_weight(j)
            self.weight_normalisation_jth(j_thweights=jth_weight)

    def matrix_dilution_init(self):
        nr_of_values = self.iplayer_size * self.oplayer_size
        #no_of_zeros = int((1 - self.dilution) * nr_of_values)
        no_of_zeros = int((1 - 0.9) * nr_of_values)

        if(no_of_zeros == 0):
            pass
        else:

            random.seed(3)
            x = random.sample(range(0, nr_of_values), no_of_zeros)
            weight_matrix = self.weight_matrix.numpy()
            for i in x:
                weight_matrix.flat[i] = 0
            self.weight_matrix = torch.from_numpy(weight_matrix)

    def matrix_dilution(self):
        if (self.eli == 0):
            nr_of_values = self.iplayer_size * self.oplayer_size
            no_of_zeros = int((1 - self.dilution) * nr_of_values)

            if (no_of_zeros == 0):
                pass

            else:
                random.seed(3)
                x = random.sample(range(0, nr_of_values), no_of_zeros)
                weight_matrix = self.weight_matrix.numpy()
                for i in x:
                    weight_matrix.flat[i] = 0
                self.weight_matrix = torch.from_numpy(weight_matrix)
        else:
            nr_of_values = self.iplayer_size * self.oplayer_size
            no_of_zeros = int((1 - self.dilution) * nr_of_values)
            weight_matrix = self.weight_matrix.numpy()
            zero_count = 0
            count = 0

            for l in range(0, nr_of_values):
                if (weight_matrix.flat[l] == 0):
                    zero_count = zero_count +1

            print("before dil zero count is {0}".format(zero_count))

            if (zero_count < no_of_zeros):


                th = (numpy.average(weight_matrix))
                print ("threshold is {0} ".format(th))

                for i in range(0, nr_of_values):

                    if ((weight_matrix.flat[i] < th) & (weight_matrix.flat[1] != 0)):
                        weight_matrix.flat[i] = 0
                        count = count + 1

                    #else:
                        #pass
            print ("The no of connections set to 0 is {0}, total zero is {1}".format(count, zero_count))
            self.weight_matrix = torch.from_numpy(weight_matrix)

    def activation_function(self):
        result_list = []
        for j in range(0, self.oplayer_size):
            jth_weights = self.j_th_weight(j)
            curr_weight = self.activate_a_node(jth_weights=jth_weights)
            result_list.append(curr_weight)

        self.oplayer = torch.FloatTensor(result_list)

    def activate_a_node(self, jth_weights):
        sum_ = 0
        for i in range(0, self.iplayer_size):
            sum_ += jth_weights[i].item() * self.iplayer[i].item()

        return sum_

    def weight_update(self):
        print("inside oja weight update")
        #print (self.weight_matrix)
        for i in range(0, self.iplayer_size):
            for j in range(0, self.oplayer_size):
                if ((self.weight_matrix[i, j]) == 0):
                    #self.weight_matrix[i, j] = 0
                    pass
                else:
                    self.weight_matrix[i, j] += self.config.ALPHA * self.oplayer[j].item() * \
                                            (self.iplayer[i].item() - self.oplayer[j].item() *
                                             self.weight_matrix[i,j])
        self.matrix_dilution()


    @property
    def oplayer_dict(self):
        oplayer_list = []
        for idx, value in enumerate(self.oplayer):
            temp_dict = dict()
            temp_dict['idx'] = idx
            temp_dict['value'] = value.item()
            oplayer_list.append(temp_dict)
        return oplayer_list

    @property
    def activation_list_ec_ca3(self) -> List:
        """
            Naive representation of the code coming in from EC2 to CA3 to make the process of understanding the
            functionality better

        :return: Activation received by CA3 directly from EC2
        :rtype List

        """
        return self.oplayer

    def extract_activation_list(self):

        sorted_oplayer = sorted(self.oplayer_dict, key=lambda k: k['value'], reverse=True)
        no_of_elem = int(self.config.OUTPUT_ACTIVATION_RATIO * self.oplayer_size)
        sorted_oplayer = sorted_oplayer[:no_of_elem]
        activated_indices = []

        for data in sorted_oplayer:
            activated_indices.append(data['idx'])

        for idx, value in enumerate(self.oplayer):
            if idx in activated_indices:
                self.oplayer[idx] = 1
            else:
                self.oplayer[idx] = 0


if __name__ == '__main__':
    pass