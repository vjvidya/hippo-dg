"""
This file implements a Hopfield network. It provides functions to
set and retrieve the network state, store patterns.

Relevant book chapters:
    - http://neuronaldynamics.epfl.ch/online/Ch17.S2.html

"""

# This file is part of the exercise code repository accompanying
# the book: Neuronal Dynamics (see http://neuronaldynamics.epfl.ch)
# located at http://github.com/EPFL-LCN/neuronaldynamics-exercises.

# This free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License 2.0 as published by the
# Free Software Foundation. You should have received a copy of the
# GNU General Public License along with the repository. If not,
# see http://www.gnu.org/licenses/.

# Should you reuse and publish the code for your own purposes,
# please cite the book or point to the webpage http://neuronaldynamics.epfl.ch.

# Wulfram Gerstner, Werner M. Kistler, Richard Naud, and Liam Paninski.
# Neuronal Dynamics: From Single Neurons to Networks and Models of Cognition.
# Cambridge University Press, 2014.

import numpy as np
import random
from dendrate_gyrus.run_dg import run_demo
from config import HopfieldNetworkConfig as hn

class HopfieldNetwork:
    """Implements a Hopfield network.

    Attributes:
        nrOfNeurons (int): Number of neurons
        weights (numpy.ndarray): nrOfNeurons x nrOfNeurons matrix of weights
        state (numpy.ndarray): current network state. matrix of shape (nrOfNeurons, nrOfNeurons)
    """

    def __init__(self, nr_neurons, weights=None, flag=0):
        """
        Constructor

        Args:
            nr_neurons (int): Number of neurons. Use a square number to get the
            visualizations properly
        """
        # math.sqrt(nr_neurons)
        self.nrOfNeurons = nr_neurons
        #set dilution 1 is fully connected
        self.dil = hn.DILUTION
        #set dil mode 1 is symmetric
        self.dilmode = hn.DILUTION_MODE
        # initialize with random state
        self.state = 2 * np.random.randint(0, 2, self.nrOfNeurons) - 1
        # initialize random weights
        self.weights = 0

        if flag != 0:
            self.weights = weights
        else:
            self.reset_weights()

        self._update_method = _get_sign_update_function()

    def reset_weights(self):
        """
        Resets the weights to random values
        """
        self.weights = 1.0 / self.nrOfNeurons * \
            (2 * np.random.rand(self.nrOfNeurons, self.nrOfNeurons) - 1)

    def set_dynamics_sign_sync(self):
        """
        sets the update dynamics to the synchronous, deterministic g(h) = sign(h) function
        """
        self._update_method = _get_sign_update_function()

    def set_dynamics_sign_async(self):
        """
        Sets the update dynamics to the g(h) =  sign(h) functions. Neurons are updated asynchronously:
        In random order, all neurons are updated sequentially
        """
        self._update_method = _get_async_sign_update_function()

    def set_dynamics_to_user_function(self, update_function):
        """
        Sets the network dynamics to the given update function

        Args:
            update_function: upd(state_t0, weights) -> state_t1.
                Any function mapping a state s0 to the next state
                s1 using a function of s0 and weights.
        """
        self._update_method = update_function



    def store_patterns(self, pattern_list):
        """
        Learns the patterns by setting the network weights. The patterns
        themselves are not stored, only the weights are updated!
        self connections are set to 0.

        Args:
            pattern_list: a nonempty list of patterns.
        """
        #for p in pattern_list:
            #print(len(p.flatten()))
        all_same_size_as_net = all(len(p.flatten()) == self.nrOfNeurons for p in pattern_list)
        if not all_same_size_as_net:
            errMsg = "Not all patterns in pattern_list have exactly the same number of states " \
                     "as this network has neurons n = {0}.".format(self.nrOfNeurons)
            raise ValueError(errMsg)

        #self.weights = np.zeros((self.nrOfNeurons, self.nrOfNeurons))

        #Weight init and dilution, square because ip and op layer are same in CA3 RC
        if (self.dil == 0):
            errMsg = "Dilution cannot be set to zero"
            raise ValueError(errMsg)

        self.weights[:] = (1/((self.nrOfNeurons * self.nrOfNeurons) * self.dil))
        self.matrix_dilution_hop()

        # textbook formula to compute the weights:
        for p in pattern_list:
            p_flat = p.flatten()

            if (self.dilmode == 0):
                for i in range(self.nrOfNeurons):
                    for k in range(self.nrOfNeurons):
                        if (self.weights[i, k] == 0):
                            pass
                        else:
                            self.weights[i, k] += p_flat[i] * p_flat[k]
            else:
                for i in range(self.nrOfNeurons):
                    for k in range(0,i):
                        if (self.weights[i, k] == 0):
                            pass
                        else:
                            self.weights[i, k] += p_flat[i] * p_flat[k]
                            self.weights[k, i] = self.weights[i, k] #symmetric

        self.weights /= self.nrOfNeurons
        # no self connections:
        np.fill_diagonal(self.weights, 0)
        #self.matrix_dilution_hop()
        self.remove_conn()

    def remove_conn(self):
        end = self.nrOfNeurons * self.nrOfNeurons
        no_of_zeros = int((1 - self.dil) * end - self.nrOfNeurons)

        if (no_of_zeros <= 0):
            pass

        else:

            if (self.dilmode == 0):
                # Asymmetric

                random.seed(3)
                x = random.sample(range(0, end), no_of_zeros)

                for i in x:
                    self.weights.flat[i] = 0

            else:

                idx = self.nrOfNeurons
                x = np.zeros(no_of_zeros, np.int)
                y = np.zeros(no_of_zeros, np.int)

                i = 0
                while i < no_of_zeros:

                    x[i] = random.randint(0, idx - 1)
                    y[i] = random.randint(0, idx - 1)
                    q = x[i]
                    r = y[i]
                    i = i + 1
                    if (q >= r):
                        i = i - 1
                    # print "\n {0}".format(i)

                j = 0
                while j < no_of_zeros:
                    m = x[j]
                    n = y[j]
                    # print ("final index {0},{1}".format(m, n))
                    self.weights[m, n] = 0
                    self.weights[n, m] = 0
                    j = j + 2




    def matrix_dilution_hop(self):

        end = self.nrOfNeurons * self.nrOfNeurons
        no_of_zeros = int((1 - self.dil) * end - self.nrOfNeurons)

        if (no_of_zeros <=0):
            pass

        else:

            if (self.dilmode == 0):
                #Asymmetric

                random.seed(3)
                x = random.sample(range(0, end), no_of_zeros)

                for i in x:
                    self.weights.flat[i] = 0

            else:

                idx = self.nrOfNeurons
                x = np.zeros(no_of_zeros, np.int)
                y = np.zeros(no_of_zeros, np.int)

                i = 0
                while i < no_of_zeros:

                    x[i] = random.randint(0, idx - 1)
                    y[i] = random.randint(0, idx - 1)
                    q = x[i]
                    r = y[i]
                    i = i + 1
                    if (q >= r):
                        i = i - 1
                    #print "\n {0}".format(i)

                j = 0
                while j < no_of_zeros:
                    m = x[j]
                    n = y[j]
                    #print ("final index {0},{1}".format(m, n))
                    self.weights[m, n] = 0
                    self.weights[n, m] = 0
                    j = j + 2


    def store_patterns_with_dg(self, pattern_list):
        all_same_size_as_net = all(len(p.flatten()) == self.nrOfNeurons for p in pattern_list)
        if not all_same_size_as_net:
            errMsg = "Not all patterns in pattern_list have exactly the same number of states " \
                     "as this network has neurons n = {0}.".format(self.nrOfNeurons)
            raise ValueError(errMsg)
        self.weights = np.zeros((self.nrOfNeurons, self.nrOfNeurons))
        # textbook formula to compute the weights:
        modified_pattern_list = []
        for p in pattern_list:
            p_flat = p.flatten()
            #print("list after flatten {0}".format(p_flat))
            for i in range(0, len(p_flat)):
                if p_flat[i] == -1:
                    p_flat[i] = 0

            op_layer = run_demo(pattern_=p_flat, flag=0)
            print("from dg")
            print(op_layer)
            if len(op_layer) != len(p_flat):
                print("op layer = {0} and input layer = {1}".format(len(op_layer), len(p_flat)))
                raise ValueError("Size mismatch, check it yo!")

            #print(f"Output Layer: {op_layer}")
            #print(f"Flattened List: {p_flat}")

            for i in range(0, len(op_layer)):
                if op_layer[i] != p_flat[i]:
                    p_flat[i] = 1
                else:
                    continue


            #print(f"After Logical Or: {p_flat}")

            for j in range(0, len(p_flat)):
                #print(p_flat[j])
                if p_flat[j] == 0:
                    p_flat[j] = -1
                else:
                    p_flat[j] = 1

            #print(f"After changing values: {p_flat}")
            print("End of pattern training")
            modified_pattern_list.append(p_flat)
            for i in range(self.nrOfNeurons):
                for k in range(self.nrOfNeurons):
                    self.weights[i, k] += p_flat[i] * p_flat[k]


        self.weights /= self.nrOfNeurons
        # no self connections:
        np.fill_diagonal(self.weights, 0)
        #self.matrix_dilution_hop()
        return modified_pattern_list

    def set_state_from_pattern(self, pattern):
        """
        Sets the neuron states to the pattern pixel. The pattern is flattened.

        Args:
            pattern: pattern
        """
        self.state = pattern.copy().flatten()

    def iterate(self):
        """Executes one timestep of the dynamics"""
        self.state = self._update_method(self.state, self.weights)

    def run(self, nr_steps=5):
        """Runs the dynamics.

        Args:
            nr_steps (float, optional): Timesteps to simulate
        """
        for i in range(nr_steps):
            # run a step
            self.iterate()

    def run_with_monitoring(self, nr_steps=5):
        """
        Iterates at most nr_steps steps. records the network state after every
        iteration

        Args:
            nr_steps:

        Returns:
            a list of 2d network states
        """
        states = list()
        states.append(self.state.copy())
        #print ("before iterate")
        #print (self.state)
        for i in range(nr_steps):
            # run a step
            self.iterate()
            #print ("inside run with monitoring, {0}".format(i))
            #print(self.state)
            states.append(self.state.copy())

        return states


def _get_sign_update_function():
    """
    for internal use

    Returns:
        A function implementing a synchronous state update using sign(h)
    """
    def upd(state_s0, weights):
        h = np.sum(weights * state_s0, axis=1)
        s1 = np.sign(h)
        # by definition, neurons have state +/-1. If the
        # sign function returns 0, we set it to +1
        idx0 = s1 == 0
        s1[idx0] = 1
        return s1
    return upd


def _get_async_sign_update_function():
    def upd(state_s0, weights):
        random_neuron_idx_list = np.random.permutation(len(state_s0))
        state_s1 = state_s0.copy()
        for i in range(len(random_neuron_idx_list)):
            rand_neuron_i = random_neuron_idx_list[i]
            h_i = np.dot(weights[:, rand_neuron_i], state_s1)
            s_i = np.sign(h_i)
            if s_i == 0:
                s_i = 1
            state_s1[rand_neuron_i] = s_i
        return state_s1
    return upd

if __name__ == "__main__":
    pass
